from abc import ABC, abstractmethod

class Car(ABC):

	@abstractmethod 
	def car_name(self):
		pass
		
class Maruti(Car):
	def car_name(self):
		print("My name is Maruti.\n")
		
class Santro(Car):
	def car_name(self):
		print ("My name is Santro.\n")
		
obj = Maruti()
obj.car_name()

objv = Santro()
objv.car_name()
