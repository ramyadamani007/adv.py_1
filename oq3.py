class Number():
	mult = 10
	
	def __init__(self,x,y):
		self.x = x
		self.y = y
	
	def add (self):
		total = self.x + self.y
		return total
		
	@classmethod	
	def multiply (cls,a):
		return (a * cls.mult)

	@staticmethod	
	def subtract (b,c):
		return (b - c)
		
				
obj = Number(10,20)
print(obj.add())
print(Number.multiply (10))
print(Number.subtract (50,30))
