import re

def valid(a):
	combi = re.compile("(0|91)?[7-9][0-9]{9}")
	return combi.match(a)

a = input("Enter a 10 digit phone number.\n")

if (valid(a)):
	print ("Valid Number.\n")     
else :
	print ("Invalid Number.\n")

