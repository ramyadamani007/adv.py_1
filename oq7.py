class employee :
	counter = 0
	
	def __init__(self,name,age) :
		self.name = name
		self.age = age

		employee.counter += 1
	
	def details(self) :
		print(self.name,self.age,"years old")

emp1 = employee('Ramya Damani',22)
emp2 = employee('Kushal Panchal',22)
emp3 = employee('Yash Patel',25)
emp4 = employee('Ronak ojha',22)

# Print the total no. of objects cretaed 
print("Total number of objects created: ",employee.counter)
