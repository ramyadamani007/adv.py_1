class Student :
	def __init__(self,name,age,blood_grp):

		# instance variable or object attributes
		self.name = name
		self.age = age
		self.blood_grp = blood_grp

	def display (self):
		print(self.name,self.age,self.blood_grp)

    
stu1 = Student("Ramya Damani",22, "O_positive")
stu2 = Student("Kushal Panchal",22, "AB_negative")
stu3 = Student('Shaurya shah',23, "O_negative")

stu1.display()
stu2.display()
stu3.display()
